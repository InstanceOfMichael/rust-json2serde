const fs = require('fs');
const argv = require('minimist')(process.argv.slice(2));
// console.dir(argv);
const inFile  = argv._[0];
const outFile = argv._[1];

if (!inFile)  throw Error('inFile required');
if (!outFile) throw Error('outFile required');

// @todo package this line and release as new npm module
var obj = JSON.parse(fs.readFileSync(inFile, 'utf8'));

const write = (ti,str) =>
{
    const tabs = ('    ').repeat(ti||0);
    console.log(tabs+str);
}

write(0,'extern crate serde_json;');
write(0,'');
write(0,'use serde_json::builder::ObjectBuilder;');
write(0,'use serde_json::Value;');
write(0,'');
write(0,'fn main() {');
write(1,'let json = ObjectBuilder::new()');

const prefixInsertOrPush = (insert,key) =>
{
    return '.' + insert + '('
        +
            (!insert.startsWith('push') ? '"'+key+'", ' : '')
    ;
}

const writeObj = (level, key1, obj, insert) =>
{
    insert = insert||'insert';
    if (obj == null)
    {
        write(level, prefixInsertOrPush(insert,key1) + 'Value::Null)');
        return;
    }
    switch(typeof obj)
    {
        case 'boolean':
        {
            write(level, prefixInsertOrPush(insert,key1) + ''+ JSON.stringify(obj) +')');
            return;
        }
        case 'string':
        {
            write(level, prefixInsertOrPush(insert,key1) + '' + JSON.stringify(obj) +')');
            return;
        }
        case 'number':
        {
            write(level, prefixInsertOrPush(insert,key1) + '' + JSON.stringify(obj) +'_f64)');
            return;
        }
        case 'object':
        {
            if (Array.isArray(obj))
            {
                write(level, prefixInsertOrPush(insert+'_array',key1) + '|builder| {');
                write(level+1,'builder');
                for (key2 in obj)
                {
                    // todo .push_object()
                    writeObj(level+2, key2, obj[key2], 'push');
                }
                write(level,'})');
            }
            else
            {
                write(level, prefixInsertOrPush(insert+'_object',key1) + '|builder| {');
                write(level+1,'builder');
                for (key2 in obj)
                {
                    writeObj(level+2, key2, obj[key2]);
                }
                write(level,'})');
            }
            return;
        }
    }
    throw Error('typeof "' + typeof obj + '"');
};

for (key in obj)
{
    writeObj(2, key, obj[key]);
}

write(1,'    .unwrap();');
write(0,'}');
write(0,'');
